# project-santorini-backend
- Reproduction of a popular multiplayer board
- Shipped TypeScript code to create the user interface for Santorini with React and hosted the application with Node and Express
- Built the backend services with Java, managing API requests and game states using NanoHTTPD
