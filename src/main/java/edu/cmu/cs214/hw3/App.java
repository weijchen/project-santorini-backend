package edu.cmu.cs214.hw3;

import edu.cmu.cs214.hw3.Board.Grid;
import edu.cmu.cs214.hw3.Player.Character;
import edu.cmu.cs214.hw3.Player.Player;
import fi.iki.elonen.NanoHTTPD;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class App extends NanoHTTPD {

    public static void main(String[] args) throws Exception {
        try {
            new App();
        } catch (IOException ioe) {
            System.out.println("Couldn't start server:\n" + ioe);
        }
    }

    private Game game;
    private List<Player> players;
    private static final int PORT_NUMBER = 1000;

    public App() throws Exception {
        super(PORT_NUMBER);
        start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
        System.out.println("\nRunning!\n");
    }

    @Override
    public Response serve(IHTTPSession session) {
        String uri = session.getUri();
        Map<String, String> params = session.getParms();
        if (uri.equals("/newGame")) {
            System.out.println("[INFO] Start new game");
            this.game = new Game();
        } else if (uri.equals("/addWorker")) {
            System.out.println("[INFO] add worker");
            int playerID = Integer.parseInt(params.get("playerID"));
            int workerID = Integer.parseInt(params.get("workerID"));
            int row = Integer.parseInt(params.get("row"));
            int col = Integer.parseInt(params.get("col"));
            this.game = this.game.addWorkerForPlayer(playerID, workerID, row, col);
        } else if (uri.equals("/move")) {
            System.out.println("[INFO] move worker");
            int workerID = Integer.parseInt(params.get("workerID"));
            int row = Integer.parseInt(params.get("row"));
            int col = Integer.parseInt(params.get("col"));
            this.game = this.game.moveWorker(workerID, row, col);
        } else if (uri.equals("/build")) {
            System.out.println("[INFO] build tower");
            int workerID = Integer.parseInt(params.get("workerID"));
            int row = Integer.parseInt(params.get("row"));
            int col = Integer.parseInt(params.get("col"));
            this.game = this.game.buildTower(workerID, row, col);
        } else if (uri.equals("/currentPlayer")) {
            System.out.println("[INFO] get current player");
            String jsonString = "{ \"curPlayerID\": " + String.valueOf(this.game.getCurPlayerID()) + "}";
            return newFixedLengthResponse(jsonString);
        } else if (uri.equals("/initializePlayer")) {
            System.out.println("[INFO] initialize player");
            int playerOneGodCard = Integer.parseInt(params.get("p1"));
            int playerTwoGodCard = Integer.parseInt(params.get("p2"));
            String p1 = Character.get(Integer.valueOf(playerOneGodCard));
            String p2 = Character.get(Integer.valueOf(playerTwoGodCard));
            String ret = "{ \"p1\": " + "\"" + p1 + "\"" + "," +
                    " \"p2\": " + "\"" + p2 + "\"" + " }";
            Player playerOne = Character.create(0, playerOneGodCard);
            Player playerTwo = Character.create(1, playerTwoGodCard);
            this.players = new ArrayList<>();
            this.players.add(playerOne);
            this.players.add(playerTwo);
            this.game = new Game(this.game.getBoard(), this.players);
            return newFixedLengthResponse(ret);
        } else if (uri.equals("/initializeWorker")) {
            System.out.println("[INFO] initialize worker");
            int playerID = Integer.parseInt(params.get("playerID"));
            int workerID = Integer.parseInt(params.get("workerID"));
            int row = Integer.parseInt(params.get("row"));
            int col = Integer.parseInt(params.get("col"));
            this.game = this.game.addWorkerForPlayer(playerID, workerID, row, col);
        } else if (uri.equals("/getMove")) {
            System.out.println("[INFO] get movable grid");
            int workerID = Integer.parseInt(params.get("workerID"));
            List<Grid> grids = this.game.getPossibleMoveForWorker(workerID);
            return newFixedLengthResponse(GameState.getCandidates(grids));
        } else if (uri.equals("/getCharge")) {
            System.out.println("[INFO] get chargeable grid");
            int workerID = Integer.parseInt(params.get("workerID"));
            List<Grid> grids = this.game.getPossibleChargeForWorker(workerID);
            return newFixedLengthResponse(GameState.getCandidates(grids));
        } else if (uri.equals("/getSwap")) {
            System.out.println("[INFO] get swappable grid");
            int workerID = Integer.parseInt(params.get("workerID"));
            List<Grid> grids = this.game.getPossibleSwapForWorker(workerID);
            return newFixedLengthResponse(GameState.getCandidates(grids));
        } else if (uri.equals("/getBuild")) {
            System.out.println("[INFO] get buildable grid");
            int workerID = Integer.parseInt(params.get("workerID"));
            List<Grid> grids = this.game.getPossibleBuildForWorker(workerID);
            return newFixedLengthResponse(GameState.getCandidates(grids));
        } else if (uri.equals("/takeTurn")) {
            this.game = this.game.takeTurn();
        } else if (uri.equals("/checkWin")) {
            System.out.println("[INFO] check win");
            boolean hasWin = this.game.hasWinner(false);
            int winnerID = this.game.getWinnerID();
            String jsonString = "{ \"hasWin\": " + hasWin + "," +
                    " \"winnerID\": " + winnerID + " }";
            return newFixedLengthResponse(jsonString);
        }

        GameState gameplay = GameState.forGame(this.game);
        return newFixedLengthResponse(gameplay.toString());
    }
}
