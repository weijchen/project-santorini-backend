package edu.cmu.cs214.hw3;

import java.util.Arrays;
import java.util.List;

import edu.cmu.cs214.hw3.Board.Board;
import edu.cmu.cs214.hw3.Board.Grid;
import edu.cmu.cs214.hw3.Board.Tower;

public final class GameState {
  private final Grid[] grids;
  private static final int BOARD_SIZE = 25;
  private static final int ROW_SIZE = 5;
  private static final int COL_SIZE = 5;

  private GameState(Grid[] grids) {
    this.grids = grids;
  }

  public static GameState forGame(Game game) {
    Grid[] grids = getGrids(game);
    return new GameState(grids);
  }

  public Grid[] getGrids() {
    return this.grids;
  }

  @Override
  public String toString() {
    return "{ \"grids\": " + Arrays.toString(this.grids) + "}";
  }

  private static Grid[] getGrids(Game game) {
    Grid[] grids = new Grid[BOARD_SIZE];
    Board board = game.getBoard();
    for (int row = 0; row < ROW_SIZE; row++) {
      for (int col = 0; col < COL_SIZE; col++) {
        boolean occupied = board.getGrid(row, col).getOccupied();
        Grid grid = board.getGrid(row, col);
        Tower levelBuilt = board.getGrid(row, col).getLevels();
        grids[ROW_SIZE * row + col] = new Grid(row, col, occupied, levelBuilt, grid.getPlayer(), grid.getWorker(),
            grid.getPlayerID(), grid.getWorkerID());
      }
    }
    return grids;
  }

  public static String getCandidates(List<Grid> candidates) {
    String jsonString = "{ \"candidates\": [";
    for (int i = 0; i < candidates.size(); i++) {
      jsonString = jsonString.concat("{ \"row\": " + candidates.get(i).getRow() + "," +
          " \"col\": " + candidates.get(i).getCol());
      if (i != candidates.size() - 1) {
        jsonString = jsonString.concat("}, ");
      } else {
        jsonString = jsonString.concat("} ");
      }
    }
    jsonString = jsonString.concat("]}");

    return jsonString;
  }
}
