package edu.cmu.cs214.hw3.Player;

import java.util.HashMap;
import java.util.Map;

public enum Character {
  Demeter(0, "Demeter"), Minotaur(1, "Minotaur"), Pan(2, "Pan"), Mortal(3, "Mortal"), Artemis(4, "Artemis"),
  Hephaestus(5, "Hephaestus"), Apollo(6, "Apollo");

  private static final Map<Integer, Character> LOOKUP_MAP = new HashMap<Integer, Character>();
  static {
    for (Character c : Character.values()) {
      LOOKUP_MAP.put(c.getID(), c);
    }
  }

  private int id;
  private String name;

  Character(int id, String name) {
    this.id = id;
    this.name = name;
  }

  private int getID() {
    return id;
  }

  private String getName() {
    return name;
  }

  public static String get(int id) {
    return LOOKUP_MAP.get(id).getName();
  }

  public static Player create(int index, int type) {
    Player curPlayer;
    switch (get(type)) {
      case "Demeter":
        curPlayer = new Demeter(index);
        break;
      case "Minotaur":
        curPlayer = new Minotaur(index);
        break;
      case "Pan":
        curPlayer = new Pan(index);
        break;
      case "Mortal":
        curPlayer = new Mortal(index);
        break;
      case "Artemis":
        curPlayer = new Artemis(index);
        break;
      case "Hephaestus":
        curPlayer = new Hephaestus(index);
        break;
      case "Apollo":
        curPlayer = new Apollo(index);
        break;
      default:
        curPlayer = new Mortal(index);
        break;
    }
    return curPlayer;
  }
}
