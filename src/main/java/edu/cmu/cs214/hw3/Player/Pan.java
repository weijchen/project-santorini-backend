package edu.cmu.cs214.hw3.Player;

import edu.cmu.cs214.hw3.Board.Board;
import edu.cmu.cs214.hw3.Board.Grid;
import edu.cmu.cs214.hw3.Board.Tower;

public class Pan extends Player {
  public Pan(int index) {
    super(index);
  }

  /**
   * moveWorker for Pan needs to check an additional winning condition
   */
  @Override
  public Board moveWorker(Board board, Worker worker, int row, int col) {
    Grid prevGrid = worker.getGrid();
    Grid nextGrid = board.getGrid(row, col);
    Tower prevGridLevel = prevGrid.getLevels();
    Tower nextGridLevel = nextGrid.getLevels();

    prevGrid.setOccupied(false);
    worker.setGrid(nextGrid);
    nextGrid.setOccupied(true, this, worker, super.getPlayerID(), worker.getWorkerID());

    // whether the player move down for more than two levels
    if (prevGridLevel.getLevel() - nextGridLevel.getLevel() >= 2) {
      super.setWin();
    }

    if (nextGridLevel.getLevel() > prevGridLevel.getLevel()
        && nextGrid.getLevels() == Tower.Third) {
      super.setWin();
    }

    return new Board(copyBoard(board));
  }
}
