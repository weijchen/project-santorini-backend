package edu.cmu.cs214.hw3.Player;

import java.util.ArrayList;
import java.util.List;

import edu.cmu.cs214.hw3.Board.Board;
import edu.cmu.cs214.hw3.Board.Grid;
import edu.cmu.cs214.hw3.Board.Tower;

public abstract class Player {
  private boolean win;
  private final int playerID;
  private List<Worker> workers;
  private static final int ROW_SIZE = 5;
  private static final int COL_SIZE = 5;

  public Player(int index) {
    this.playerID = index;
    this.workers = new ArrayList<>();
  }

  /**
   * Initialize Worker with given information.
   * 
   * @param board
   * @param index
   * @param row
   * @param col
   * @return boolean Initialization is valid
   */
  public boolean initializeWorker(Board board, int index, int row, int col) {
    if (!board.checkGridIsValid(row, col)) {
      return false;
    }
    Grid grid = board.getGrid(row, col);
    grid.setOccupied(true, this, null, getPlayerID(), index);
    Worker newWorker = new Worker(this.playerID, index, grid);
    this.workers.add(newWorker);
    grid.setWorker(newWorker);
    return true;
  }

  public boolean getWin() {
    return win;
  }

  public void setWin() {
    this.win = true;
  }

  public int getPlayerID() {
    return playerID;
  }

  public List<Worker> getWorkers() {
    return workers;
  }

  public Worker getWorkerWithID(int workerID) {
    return workers.get(workerID);
  }

  public boolean checkMove(Board board, Worker worker, int row, int col) {
    Grid prevGrid = worker.getGrid();
    List<Grid> canMoveTo = board.possibleMoves(prevGrid.getRow(),
        prevGrid.getCol());

    boolean found = false;
    for (Grid grid : canMoveTo) {
      if (grid.getRow() == row && grid.getCol() == col) {
        found = true;
      }
    }

    return found;
  }

  /**
   * Move Worker to given row and col.
   *
   * @param board
   * @param worker
   * @param row
   * @param col
   * @return Updated board
   */
  public Board moveWorker(Board board, Worker worker, int row, int col) {
    Grid prevGrid = worker.getGrid();
    Grid nextGrid = board.getGrid(row, col);

    prevGrid.setOccupied(false);
    worker.setGrid(nextGrid);
    nextGrid.setOccupied(true, this, worker, getPlayerID(), worker.getWorkerID());
    if (nextGrid.getLevels().getLevel() > prevGrid.getLevels().getLevel()
        && nextGrid.getLevels() == Tower.Third) {
      setWin();
    }

    return new Board(copyBoard(board));
  }

  public boolean checkBuild(Board board, Worker worker, int row, int col) {
    Grid prevGrid = worker.getGrid();
    List<Grid> canBuildOn = board.possibleBuilds(prevGrid.getRow(),
        prevGrid.getCol());

    boolean found = false;
    for (Grid grid : canBuildOn) {
      if (grid.getRow() == row && grid.getCol() == col) {
        found = true;
      }
    }

    return found;
  }

  /**
   * Let Worker build on given row and col.
   *
   * @param board
   * @param worker
   * @param row
   * @param col
   * @return Updated board
   */
  public Board buildTower(Board board, Worker worker, int row, int col) {
    board.getGrid(row, col).buildTower();

    return new Board(copyBoard(board));
  }

  protected Grid[][] copyBoard(Board board) {
    Grid[][] newBoard = new Grid[ROW_SIZE][COL_SIZE];
    int length = newBoard.length;
    for (int i = 0; i < length; i++) {
      for (int j = 0; j < length; j++) {
        newBoard[i][j] = board.getGrid(i, j);
      }
    }

    return newBoard;
  }
}
