package edu.cmu.cs214.hw3.Player;

import edu.cmu.cs214.hw3.Board.Board;
import edu.cmu.cs214.hw3.Board.Grid;

public class Demeter extends Player {
  private Grid prevBuild;

  public Demeter(int index) {
    super(index);
    prevBuild = null;
  }

  /**
   * buildTower method for Demeter is going to modify the state of the prevBuild
   * grid and based on whether entered row and col are equal to the prevBuild, the
   * system will decide whether to enable the optional second build
   */
  @Override
  public Board buildTower(Board board, Worker worker, int row, int col) {
    // has previous build
    if (prevBuild != null) {

      // return false if new build is the same as the previous build
      if ((prevBuild.getRow() == row && prevBuild.getCol() == col)) {
        prevBuild = null;
        return board;
      } else {
        prevBuild = null;
        return super.buildTower(board, worker, row, col);
      }
    }

    prevBuild = board.getGrid(row, col);
    return super.buildTower(board, worker, row, col);
  }
}
