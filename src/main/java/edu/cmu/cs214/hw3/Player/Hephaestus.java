package edu.cmu.cs214.hw3.Player;

import edu.cmu.cs214.hw3.Board.Board;
import edu.cmu.cs214.hw3.Board.Grid;
import edu.cmu.cs214.hw3.Board.Tower;

public class Hephaestus extends Player {
  private Grid prevBuild;

  public Hephaestus(int index) {
    super(index);
    prevBuild = null;
  }

  /**
   * buildTower method for Hephaestus is going to modify the state of the
   * prevBuild
   * grid and based on whether entered row and col are equal to the prevBuild, the
   * system will decide whether to enable the optional second build
   */
  @Override
  public Board buildTower(Board board, Worker worker, int row, int col) {
    // has previous build
    if (prevBuild != null) {

      // return false if new build is the same as the previous build
      if ((prevBuild.getRow() == row && prevBuild.getCol() == col)) {
        if (prevBuild.getLevels() == Tower.Third) {
          prevBuild = null;
          return board;
        }
        prevBuild = null;
        return super.buildTower(board, worker, row, col);
      } else {
        prevBuild = null;
        return board;
      }
    }

    prevBuild = board.getGrid(row, col);
    return super.buildTower(board, worker, row, col);
  }
}
