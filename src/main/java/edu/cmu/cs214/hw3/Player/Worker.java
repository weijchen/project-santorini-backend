package edu.cmu.cs214.hw3.Player;

import edu.cmu.cs214.hw3.Board.Grid;

public class Worker {
  private int playerID;
  private int workerID;
  private Grid grid;

  public Worker(int playerID, int workerID, Grid grid) {
    this.playerID = playerID;
    this.workerID = workerID;
    this.grid = grid;
  }

  public int getPlayerID() {
    return playerID;
  }

  public int getWorkerID() {
    return workerID;
  }

  public Grid getGrid() {
    return this.grid;
  }

  public void setGrid(Grid newGrid) {
    this.grid = newGrid;
  }
}
