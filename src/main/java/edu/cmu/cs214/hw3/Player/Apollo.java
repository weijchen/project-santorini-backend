package edu.cmu.cs214.hw3.Player;

import java.util.List;

import edu.cmu.cs214.hw3.Board.Board;
import edu.cmu.cs214.hw3.Board.Grid;
import edu.cmu.cs214.hw3.Board.Tower;

public class Apollo extends Player {
  public Apollo(int index) {
    super(index);
  }

  @Override
  public boolean checkMove(Board board, Worker worker, int row, int col) {
    Grid prevGrid = worker.getGrid();
    List<Grid> canMoveTo = board.possibleMoves(prevGrid.getRow(),
        prevGrid.getCol());

    boolean found = false;
    for (Grid grid : canMoveTo) {
      if (grid.getRow() == row && grid.getCol() == col) {
        found = true;
      }
    }

    List<Grid> canSwapTo = board.possibleSwaps(prevGrid.getRow(), prevGrid.getCol(), getPlayerID());
    for (Grid grid : canSwapTo) {
      if (grid.getRow() == row && grid.getCol() == col) {
        found = true;

      }
    }

    return found;
  }

  /**
   * moveWorker for Apollo consists of two rules: 1. Original movement; 2.
   * Chargeable movement (reverse one)
   */
  @Override
  public Board moveWorker(Board board, Worker worker, int row, int col) {
    Grid prevGrid = worker.getGrid();
    Grid nextGrid = board.getGrid(row, col);

    Player swappedPlayer = nextGrid.getPlayer();
    Worker swappedWorker = nextGrid.getWorker();
    int swappedPlayerID = nextGrid.getPlayerID();
    int swappedWorkerID = nextGrid.getWorkerID();

    if (swappedPlayer != null && swappedPlayer.getPlayerID() != getPlayerID() && swappedWorkerID != -1) {
      Grid swapToGrid = board.getGrid(nextGrid.getRow(), nextGrid.getCol());
      swapToGrid.setOccupied(true, this, worker, this.getPlayerID(), worker.getWorkerID());
      prevGrid.setOccupied(true, swappedPlayer, swappedWorker, swappedPlayerID, swappedWorkerID);
      swappedWorker.setGrid(prevGrid);
    }

    if (swappedPlayer == null) {
      prevGrid.setOccupied(false);
    }
    worker.setGrid(nextGrid);
    nextGrid.setOccupied(true, this, worker, getPlayerID(), worker.getWorkerID());
    if (nextGrid.getLevels().getLevel() > prevGrid.getLevels().getLevel()
        && nextGrid.getLevels() == Tower.Third) {
      setWin();
    }

    return new Board(copyBoard(board));
  }
}
