package edu.cmu.cs214.hw3.Player;

import java.util.List;

import edu.cmu.cs214.hw3.Board.Board;
import edu.cmu.cs214.hw3.Board.Grid;
import edu.cmu.cs214.hw3.Board.Tower;

public class Minotaur extends Player {
  public Minotaur(int index) {
    super(index);
  }

  @Override
  public boolean checkMove(Board board, Worker worker, int row, int col) {
    Grid prevGrid = worker.getGrid();
    List<Grid> canMoveTo = board.possibleMoves(prevGrid.getRow(),
        prevGrid.getCol());

    boolean found = false;
    for (Grid grid : canMoveTo) {
      if (grid.getRow() == row && grid.getCol() == col) {
        found = true;
      }
    }

    List<Grid> canChargeTo = board.possibleCharges(prevGrid.getRow(), prevGrid.getCol(), getPlayerID());
    for (Grid grid : canChargeTo) {
      if (grid.getRow() == row && grid.getCol() == col) {
        found = true;

      }
    }

    return found;
  }

  /**
   * moveWorker for Minotaur consists of two rules: 1. Original movement; 2.
   * Chargeable movement
   */
  @Override
  public Board moveWorker(Board board, Worker worker, int row, int col) {
    Grid prevGrid = worker.getGrid();
    Grid nextGrid = board.getGrid(row, col);

    int rowOffset = nextGrid.getRow() - prevGrid.getRow();
    int colOffset = nextGrid.getCol() - prevGrid.getCol();
    Player forcedPlayer = nextGrid.getPlayer();
    Worker forcedWorker = nextGrid.getWorker();
    int forcedPlayerID = nextGrid.getPlayerID();
    int forcedWorkerID = nextGrid.getWorkerID();

    if (forcedPlayerID != getPlayerID() && forcedWorkerID != -1) {
      Grid forcedToGrid = board.getGrid(nextGrid.getRow() + rowOffset, nextGrid.getCol() + colOffset);
      nextGrid.setOccupied(false);
      forcedToGrid.setOccupied(true, forcedPlayer, forcedWorker, forcedPlayerID, forcedWorkerID);
      forcedWorker.setGrid(forcedToGrid);
    }

    prevGrid.setOccupied(false);
    worker.setGrid(nextGrid);
    nextGrid.setOccupied(true, this, worker, getPlayerID(), worker.getWorkerID());
    if (nextGrid.getLevels().getLevel() > prevGrid.getLevels().getLevel()
        && nextGrid.getLevels() == Tower.Third) {
      setWin();
    }

    return new Board(copyBoard(board));
  }
}
