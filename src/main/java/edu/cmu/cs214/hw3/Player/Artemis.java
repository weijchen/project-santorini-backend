package edu.cmu.cs214.hw3.Player;

import edu.cmu.cs214.hw3.Board.Board;
import edu.cmu.cs214.hw3.Board.Grid;

public class Artemis extends Player {
  private Grid prevGrid;

  public Artemis(int index) {
    super(index);
    prevGrid = null;
  }

  /**
   * movwWorker method for Artemis is going to modify the state of the prevMove
   * grid and based on whether entered row and col are equal to the prevMove, the
   * system will decide whether to enable the optional second move 
   */
  @Override
  public Board moveWorker(Board board, Worker worker, int row, int col) {
    // has previous move
    if (prevGrid != null) {

      // return false if new move is the same as the previous move
      if ((prevGrid.getRow() == row && prevGrid.getCol() == col)) {
        prevGrid = null;
        return board;
      } else {
        prevGrid = null;
        return super.moveWorker(board, worker, row, col);
      }
    }

    prevGrid = worker.getGrid();
    return super.moveWorker(board, worker, row, col);
  }
}
