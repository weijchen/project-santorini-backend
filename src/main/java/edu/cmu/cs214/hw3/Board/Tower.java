package edu.cmu.cs214.hw3.Board;

public enum Tower {
  None(0), First(1), Second(2), Third(3), Dome(4);

  private int level;

  Tower(int level) {
    this.level = level;
  }

  public int getLevel() {
    return level;
  }
}
