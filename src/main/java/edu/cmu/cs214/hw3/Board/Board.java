package edu.cmu.cs214.hw3.Board;

import java.util.ArrayList;
import java.util.List;

public class Board {
  private static final int BOARD_SIZE = 5;
  private Grid[][] grids;

  public Board() {
    this.grids = new Grid[BOARD_SIZE][BOARD_SIZE];

    for (int i = 0; i < this.grids.length; i++) {
      for (int j = 0; j < this.grids[i].length; j++) {
        this.grids[i][j] = new Grid(i, j, false, Tower.None);
      }
    }
  }

  public Board(Grid[][] grids) {
    this.grids = grids;
  }

  public Grid getGrid(int row, int col) {
    return grids[row][col];
  }

  /**
   * Check the given position is valid on the board (within available range and
   * not being occupied).
   * 
   * @param row
   * @param col
   * @return boolean Whether the given grid is valid
   */
  public boolean checkGridIsValid(int row, int col) {
    return checkGridIsOnBoard(row, col) && (!checkGridIsOccupied(row, col));
  }

  /**
   * Check whether the given position lies within the board
   * 
   * @param row
   * @param col
   * @return boolean Whether the given grid lies within the board
   */
  private boolean checkGridIsOnBoard(int row, int col) {
    return (row >= 0) && (row < BOARD_SIZE) && (col >= 0) && (col < BOARD_SIZE);
  }

  /**
   * Check whether the given position is occupied
   * 
   * @param row
   * @param col
   * @return boolean Whether the given grid is occupied
   */
  private boolean checkGridIsOccupied(int row, int col) {
    return this.grids[row][col].getOccupied();
  }

  /**
   * Check whether the given position is chargeable. Rules are grid is on the
   * board, grid is valid, and the worker on the grid comes from another player
   * 
   * @param row
   * @param col
   * @param newRow
   * @param newCol
   * @param chargeToRow
   * @param chargeToCol
   * @param playerID
   * @return boolean Whether the grid is chargeable
   */
  private boolean checkChargeIsValid(int row, int col, int newRow, int newCol, int chargeToRow, int chargeToCol,
      int playerID) {

    if (!checkGridIsOnBoard(newRow, newCol) || !checkGridIsValid(chargeToRow, chargeToCol))
      return false;

    int curGridPlayerID = this.grids[newRow][newCol].getPlayerID();
    return curGridPlayerID != playerID && curGridPlayerID != -1;
  }

  /**
   * Check whether the given position can be swap. Rules are grid is on the
   * board, grid is valid, and has a worker on the grid comes from another player
   * 
   * @param row
   * @param col
   * @param newRow
   * @param newCol
   * @param chargeToRow
   * @param chargeToCol
   * @param playerID
   * @return boolean Whether the grid is chargeable
   */
  private boolean checkSwapIsValid(int row, int col, int newRow, int newCol,
      int playerID) {

    if (!checkGridIsOnBoard(newRow, newCol))
      return false;

    if (this.grids[newRow][newCol].getWorkerID() == -1)
      return false;

    int curGridPlayerID = this.grids[newRow][newCol].getPlayerID();
    return curGridPlayerID != playerID && curGridPlayerID != -1;
  }

  /**
   * Find possible moves when starting from the given position.
   * 
   * @param row
   * @param col
   * @return All accessible position on the board.
   */
  public List<Grid> possibleMoves(int row, int col) {
    int[][] dir = { { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 }, { 1, 1 }, { 1, -1 }, { -1, -1 }, { -1, 1 } };
    List<Grid> possiblePoints = new ArrayList<Grid>();
    for (int i = 0; i < dir.length; i++) {
      int newRow = row + dir[i][0];
      int newCol = col + dir[i][1];

      if (checkGridIsValid(newRow, newCol)) {
        Grid curGrid = getGrid(row, col);
        Tower curGridLevel = curGrid.getLevels();
        Grid nextGrid = getGrid(newRow, newCol);
        Tower nextGridLevel = nextGrid.getLevels();

        if ((nextGridLevel.getLevel() - curGridLevel.getLevel() == 1)
            || nextGridLevel.getLevel() <= curGridLevel.getLevel()) {
          possiblePoints.add(nextGrid);
        }
      }
    }
    return possiblePoints;
  }

  /**
   * Find possible charges when starting from the given position and with given
   * playerID
   * 
   * @param row
   * @param col
   * @param playerID
   * @return All possible chargeable grid with workers from another player
   */
  public List<Grid> possibleCharges(int row, int col, int playerID) {
    int[][] dir = { { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 }, { 1, 1 }, { 1, -1 }, { -1, -1 }, { -1, 1 } };
    List<Grid> possiblePoints = new ArrayList<Grid>();
    for (int i = 0; i < dir.length; i++) {
      int rowOffset = dir[i][0];
      int colOffset = dir[i][1];
      int newRow = row + rowOffset;
      int newCol = col + colOffset;

      if (checkChargeIsValid(row, col, newRow, newCol, newRow + rowOffset, newCol + colOffset, playerID)) {
        Grid curGrid = getGrid(row, col);
        Tower curGridLevel = curGrid.getLevels();
        Grid nextGrid = getGrid(newRow, newCol);
        Tower nextGridLevel = nextGrid.getLevels();

        if ((nextGridLevel.getLevel() - curGridLevel.getLevel() == 1)
            || nextGridLevel.getLevel() <= curGridLevel.getLevel()) {
          possiblePoints.add(nextGrid);
        }
      }
    }
    return possiblePoints;
  }

  /**
   * Find possible swap when starting from the given position and with given
   * playerID
   * 
   * @param row
   * @param col
   * @param playerID
   * @return All possible swap grid with workers from another player
   */
  public List<Grid> possibleSwaps(int row, int col, int playerID) {
    int[][] dir = { { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 }, { 1, 1 }, { 1, -1 }, { -1, -1 }, { -1, 1 } };
    List<Grid> possiblePoints = new ArrayList<Grid>();
    for (int i = 0; i < dir.length; i++) {
      int rowOffset = dir[i][0];
      int colOffset = dir[i][1];
      int newRow = row + rowOffset;
      int newCol = col + colOffset;

      if (checkSwapIsValid(row, col, newRow, newCol, playerID)) {
        Grid curGrid = getGrid(row, col);
        Tower curGridLevel = curGrid.getLevels();
        Grid nextGrid = getGrid(newRow, newCol);
        Tower nextGridLevel = nextGrid.getLevels();

        if ((nextGridLevel.getLevel() - curGridLevel.getLevel() == 1)
            || nextGridLevel.getLevel() <= curGridLevel.getLevel()) {
          possiblePoints.add(nextGrid);
        }
      }
    }
    return possiblePoints;
  }

  /**
   * Find grids that can be built from the given position.
   * 
   * @param row
   * @param col
   * @return All position that can be built on the board.
   */
  public List<Grid> possibleBuilds(int row, int col) {
    int[][] dir = { { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 }, { 1, 1 }, { 1, -1 }, { -1, -1 }, { -1, 1 } };
    List<Grid> possiblePoints = new ArrayList<Grid>();
    for (int i = 0; i < dir.length; i++) {
      int newRow = row + dir[i][0];
      int newCol = col + dir[i][1];
      if (checkGridIsValid(newRow, newCol)) {
        Grid nextGrid = getGrid(newRow, newCol);
        possiblePoints.add(nextGrid);
      }
    }
    return possiblePoints;
  }
}
