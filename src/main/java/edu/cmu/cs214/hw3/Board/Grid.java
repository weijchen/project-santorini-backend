package edu.cmu.cs214.hw3.Board;

import edu.cmu.cs214.hw3.Player.Player;
import edu.cmu.cs214.hw3.Player.Worker;

public class Grid {
  private final int row;
  private final int col;
  private boolean isOccupied;
  private Tower levelBeingBuilt;
  private Player player;
  private Worker worker;
  private int playerID;
  private int workerID;

  public Grid(int row, int col, boolean occupied, Tower levels, Player player, Worker worker, int playerID,
      int workerID) {
    this.row = row;
    this.col = col;
    this.isOccupied = occupied;
    this.levelBeingBuilt = levels;
    this.player = player;
    this.worker = worker;
    this.playerID = playerID;
    this.workerID = workerID;
  }

  public Grid(int row, int col, boolean occupied, Tower levels) {
    this(row, col, occupied, levels, null, null, -1, -1);
  }

  public boolean getOccupied() {
    return isOccupied;
  }

  public Tower getLevels() {
    return levelBeingBuilt;
  }

  public int getRow() {
    return this.row;
  }

  public int getCol() {
    return this.col;
  }

  /**
   * set the grid as occupied and denote which player and worker has it
   * 
   * @param occupied
   * @param player
   * @param worker
   * @param playerID
   * @param workerID
   */
  public void setOccupied(boolean occupied, Player player, Worker worker, int playerID, int workerID) {
    this.isOccupied = occupied;
    this.player = player;
    this.worker = worker;
    this.playerID = playerID;
    this.workerID = workerID;
  }

  public void setOccupied(boolean occupied) {
    setOccupied(occupied, null, null, -1, -1);
  }

  public Player getPlayer() {
    return this.player;
  }

  public Worker getWorker() {
    return this.worker;
  }

  public void setWorker(Worker worker) {
    this.worker = worker;
  }

  public int getPlayerID() {
    return this.playerID;
  }

  public int getWorkerID() {
    return this.workerID;
  }

  /**
   * Update levelBeingBuilt status based on current value. Set occupied to true
   * once a dome is placed.
   */
  public void buildTower() {
    switch (levelBeingBuilt) {
      case None:
        levelBeingBuilt = Tower.First;
        break;
      case First:
        levelBeingBuilt = Tower.Second;
        break;
      case Second:
        levelBeingBuilt = Tower.Third;
        break;
      case Third:
        levelBeingBuilt = Tower.Dome;
        setOccupied(true);
        break;
      default:
        break;
    }
  }

  @Override
  public String toString() {
    return "{ \"row\": " + this.row + "," +
        " \"col\": " + this.col + "," +
        " \"occupied\": " + this.isOccupied + "," +
        " \"level\": " + this.levelBeingBuilt.getLevel() + "," +
        " \"playerID\": " + this.playerID + "," +
        " \"workerID\": " + this.workerID + " }";
  }
}
