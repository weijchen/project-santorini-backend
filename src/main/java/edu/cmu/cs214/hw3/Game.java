package edu.cmu.cs214.hw3;

import java.util.ArrayList;
import java.util.List;

import edu.cmu.cs214.hw3.Board.Board;
import edu.cmu.cs214.hw3.Board.Grid;
import edu.cmu.cs214.hw3.Player.Player;
import edu.cmu.cs214.hw3.Player.Worker;

public class Game {
  private int curPlayerID;
  private int winnerID;
  private final Board board;
  private final List<Player> players;
  private final List<Game> history;

  public Game() {
    this(new Board(), List.of(), 0, List.of());
  }

  public Game(Board board, List<Player> players) {
    this(board, players, 0);
  }

  public Game(Board board, List<Player> players, int curPlayerID) {
    this(board, players, curPlayerID, List.of());
  }

  public Game(Board board, List<Player> players, int curPlayerID, List<Game> history) {
    this.board = board;
    this.players = players;
    this.curPlayerID = curPlayerID;
    this.winnerID = -1;
    this.history = history;
  }

  public Board getBoard() {
    return board;
  }

  public List<Player> getPlayers() {
    return players;
  }

  public Player getPlayerWithID(int id) {
    return players.get(id);
  }

  public int getCurPlayerID() {
    return curPlayerID;
  }

  public Player getCurPlayer() {
    return getPlayerWithID(this.curPlayerID);
  }

  private void setWinner(Player player) {
    this.winnerID = player.getPlayerID();
  }

  public int getWinnerID() {
    return this.winnerID;
  }

  /**
   * add worker for player and initialize the worker's starting grid on the board
   * 
   * @param playerID
   * @param workerID
   * @param row
   * @param col
   * @return {@link Game}
   */
  public Game addWorkerForPlayer(int playerID, int workerID, int row, int col) {
    Player player = getPlayerWithID(playerID);
    player.initializeWorker(this.board, workerID, row, col);
    return new Game(this.board, this.players);
  }

  /**
   * get moveable grids for given worker
   * 
   * @param workerID
   * @return List<Grid> a list of moveable grids
   */
  public List<Grid> getPossibleMoveForWorker(int workerID) {
    Player curPlayer = getCurPlayer();
    Worker curWorker = curPlayer.getWorkerWithID(workerID);
    Grid curGrid = curWorker.getGrid();
    return this.board.possibleMoves(curGrid.getRow(), curGrid.getCol());
  }

  /**
   * get buildable grids for given worker
   * 
   * @param workerID
   * @return List<Grid> a list of buildable grids
   */
  public List<Grid> getPossibleBuildForWorker(int workerID) {
    Player curPlayer = getCurPlayer();
    Worker curWorker = curPlayer.getWorkerWithID(workerID);
    Grid curGrid = curWorker.getGrid();
    return this.board.possibleBuilds(curGrid.getRow(), curGrid.getCol());
  }

  /**
   * get chargeable grids for given worker
   * 
   * @param workerID
   * @return List<Grid> a list of chargeable grids
   */
  public List<Grid> getPossibleChargeForWorker(int workerID) {
    Player curPlayer = getCurPlayer();
    Worker curWorker = curPlayer.getWorkerWithID(workerID);
    Grid curGrid = curWorker.getGrid();
    return this.board.possibleCharges(curGrid.getRow(), curGrid.getCol(), getCurPlayerID());
  }

  /**
   * get chargeable grids for given worker
   * 
   * @param workerID
   * @return List<Grid> a list of chargeable grids
   */
  public List<Grid> getPossibleSwapForWorker(int workerID) {
    Player curPlayer = getCurPlayer();
    Worker curWorker = curPlayer.getWorkerWithID(workerID);
    Grid curGrid = curWorker.getGrid();
    return this.board.possibleSwaps(curGrid.getRow(), curGrid.getCol(), getCurPlayerID());
  }

  public Game moveWorker(int workerID, int row, int col) {
    if (this.hasWinner(false)) {
      return this;
    }

    Player curPlayer = getCurPlayer();
    Worker curWorker = curPlayer.getWorkerWithID(workerID);
    if (!curPlayer.checkMove(board, curWorker, row, col)) {
      return this;
    }

    List<Game> newHistory = new ArrayList<>(this.history);
    newHistory.add(this);

    return new Game(curPlayer.moveWorker(this.board, curWorker, row, col), this.players, getCurPlayerID(), newHistory);
  }

  public Game buildTower(int workerID, int row, int col) {
    if (this.hasWinner(false))
      return this;

    Player curPlayer = getCurPlayer();
    Worker curWorker = curPlayer.getWorkerWithID(workerID);
    if (!curPlayer.checkBuild(board, curWorker, row, col)) {
      return this;
    }

    List<Game> newHistory = new ArrayList<>(this.history);
    newHistory.add(this);
    return new Game(curPlayer.buildTower(this.board, curWorker, row, col), this.players, getCurPlayerID(), newHistory);
  }

  /**
   * Checker for game winner
   * 
   * @param showResult
   * @return boolean Whether the game has a winner
   */
  public boolean hasWinner(boolean showResult) {
    for (Player player : players) {
      if (player.getWin()) {
        if (showResult) {
          System.out.println("Player: " + (player.getPlayerID() + 1) + " wins the game!");
        }
        setWinner(player);
        return true;
      }
    }
    return false;
  }

  public Game takeTurn() {
    curPlayerID = curPlayerID == 0 ? 1 : 0;
    return this;
  }

  public void updateWorkerToGrid(int playerID, int workerID, Grid grid) {
    Worker targetWorker = getPlayerWithID(playerID).getWorkerWithID(workerID);
    targetWorker.setGrid(grid);
  }
}
