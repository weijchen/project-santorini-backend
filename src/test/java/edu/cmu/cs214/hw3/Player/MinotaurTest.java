package edu.cmu.cs214.hw3.Player;

import org.junit.Before;
import org.junit.Test;

import edu.cmu.cs214.hw3.Game;
import edu.cmu.cs214.hw3.Board.Board;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;

public class MinotaurTest {
  Game game;
  Board board;
  List<Player> players = new ArrayList<>();

  @Before
  public void setUp() throws Exception {
    board = new Board();
    Player playerOne = new Minotaur(0);
    Player playerTwo = new Mortal(1);
    players.add(playerOne);
    players.add(playerTwo);
    this.game = new Game(board, players);
    this.game = this.game.addWorkerForPlayer(0, 0, 0, 0);
    this.game = this.game.addWorkerForPlayer(0, 1, 0, 1);
    this.game = this.game.addWorkerForPlayer(1, 0, 1, 0);
    this.game = this.game.addWorkerForPlayer(1, 1, 1, 1);
  }

  /*
   * test Minotaur can correctly push other player's workers straightly
   */
  @Test
  public void testPushStraight() {
    assertEquals(this.board.possibleCharges(0, 0, 0).size(), 2);
    assertEquals(this.board.getGrid(1, 0).getPlayerID(), 1);
    assertEquals(this.board.getGrid(1, 0).getWorkerID(), 0);
    assertEquals(this.board.getGrid(2, 0).getPlayerID(), -1);
    assertEquals(this.board.getGrid(2, 0).getWorkerID(), -1);
    this.game.moveWorker(0, 1, 0);
    assertEquals(this.board.getGrid(1, 0).getPlayerID(), 0);
    assertEquals(this.board.getGrid(1, 0).getWorkerID(), 0);
    assertEquals(this.board.getGrid(2, 0).getPlayerID(), 1);
    assertEquals(this.board.getGrid(2, 0).getWorkerID(), 0);
  }

  /*
   * test Minotaur can correctly push other player's workers diagonally
   */
  @Test
  public void testPushDiagonal() {
    assertEquals(this.board.possibleCharges(0, 0, 0).size(), 2);
    assertEquals(this.board.getGrid(1, 1).getPlayerID(), 1);
    assertEquals(this.board.getGrid(1, 1).getWorkerID(), 1);
    assertEquals(this.board.getGrid(2, 2).getPlayerID(), -1);
    assertEquals(this.board.getGrid(2, 2).getWorkerID(), -1);
    this.game.moveWorker(0, 1, 1);
    assertEquals(this.board.getGrid(1, 1).getPlayerID(), 0);
    assertEquals(this.board.getGrid(1, 1).getWorkerID(), 0);
    assertEquals(this.board.getGrid(2, 2).getPlayerID(), 1);
    assertEquals(this.board.getGrid(2, 2).getWorkerID(), 1);
  }

  /*
   * test Minotaur cannot push a worker of its own
   */
  @Test
  public void testPushSelfWorker() {
    assertEquals(this.board.possibleCharges(0, 0, 0).size(), 2);
    assertEquals(this.board.getGrid(0, 1).getPlayerID(), 0);
    assertEquals(this.board.getGrid(0, 1).getWorkerID(), 1);
    assertEquals(this.board.getGrid(0, 2).getPlayerID(), -1);
    assertEquals(this.board.getGrid(0, 2).getWorkerID(), -1);
    this.game = this.game.moveWorker(0, 0, 1);
    assertEquals(this.board.getGrid(0, 1).getPlayerID(), 0);
    assertEquals(this.board.getGrid(0, 1).getWorkerID(), 1);
    assertEquals(this.board.getGrid(0, 2).getPlayerID(), -1);
    assertEquals(this.board.getGrid(0, 2).getWorkerID(), -1);
  }

  /*
   * test Minotaur cannot push a worker to an occupied grid (by self workers)
   */
  @Test
  public void testPushToOccupiedOne() {
    this.game = this.game.moveWorker(1, 1, 2);
    this.game = this.game.moveWorker(1, 2, 2);
    assertEquals(this.board.getGrid(1, 1).getPlayerID(), 1);
    assertEquals(this.board.getGrid(1, 1).getWorkerID(), 1);
    assertEquals(this.board.getGrid(2, 2).getPlayerID(), 0);
    assertEquals(this.board.getGrid(2, 2).getWorkerID(), 1);
    this.game = this.game.moveWorker(0, 1, 1);
    assertEquals(this.board.getGrid(1, 1).getPlayerID(), 1);
    assertEquals(this.board.getGrid(1, 1).getWorkerID(), 1);
    assertEquals(this.board.getGrid(2, 2).getPlayerID(), 0);
    assertEquals(this.board.getGrid(2, 2).getWorkerID(), 1);
  }

  /*
   * test Minotaur cannot push a worker to an occupied grid (by other's workers)
   */
  @Test
  public void testPushToOccupiedTwo() {
    this.game.takeTurn();
    this.game = this.game.moveWorker(0, 2, 1);
    this.game = this.game.moveWorker(0, 2, 2);
    assertEquals(this.board.getGrid(1, 1).getPlayerID(), 1);
    assertEquals(this.board.getGrid(1, 1).getWorkerID(), 1);
    assertEquals(this.board.getGrid(2, 2).getPlayerID(), 1);
    assertEquals(this.board.getGrid(2, 2).getWorkerID(), 0);
    this.game = this.game.moveWorker(0, 1, 1);
    assertEquals(this.board.getGrid(1, 1).getPlayerID(), 1);
    assertEquals(this.board.getGrid(1, 1).getWorkerID(), 1);
    assertEquals(this.board.getGrid(2, 2).getPlayerID(), 1);
    assertEquals(this.board.getGrid(2, 2).getWorkerID(), 0);
  }

  /*
   * test Minotaur's pushing will not make other win the game
   */
  @Test
  public void testPushOtherToThirdNotWin() {
    this.game = this.game.moveWorker(1, 1, 2);
    this.game = this.game.buildTower(1, 2, 2);
    this.game = this.game.buildTower(1, 2, 2);
    this.game = this.game.buildTower(1, 2, 2);
    assertEquals(this.board.getGrid(2, 2).getLevels().getLevel(), 3);
    this.game = this.game.moveWorker(0, 1, 1);
    assertEquals(this.board.getGrid(2, 2).getPlayerID(), 1);
    assertEquals(this.board.getGrid(2, 2).getWorkerID(), 1);
    assertFalse(this.game.hasWinner(false));
  }
}
