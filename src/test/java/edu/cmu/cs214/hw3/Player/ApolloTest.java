package edu.cmu.cs214.hw3.Player;

import org.junit.Before;
import org.junit.Test;

import edu.cmu.cs214.hw3.Game;
import edu.cmu.cs214.hw3.Board.Board;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

public class ApolloTest {
  Game game;
  Board board;
  List<Player> players = new ArrayList<>();

  @Before
  public void setUp() throws Exception {
    board = new Board();
    Player playerOne = new Apollo(0);
    Player playerTwo = new Mortal(1);
    players.add(playerOne);
    players.add(playerTwo);
    this.game = new Game(board, players);
    this.game = this.game.addWorkerForPlayer(0, 0, 0, 0);
    this.game = this.game.addWorkerForPlayer(0, 1, 0, 1);
    this.game = this.game.addWorkerForPlayer(1, 0, 1, 0);
    this.game = this.game.addWorkerForPlayer(1, 1, 1, 1);
  }

  /*
   * test Apollo can correctly swap other player's workers straightly
   */
  @Test
  public void testPushStraight() {
    assertEquals(this.board.possibleSwaps(0, 0, 0).size(), 2);
    assertEquals(this.board.getGrid(1, 0).getPlayerID(), 1);
    assertEquals(this.board.getGrid(1, 0).getWorkerID(), 0);
    this.game.moveWorker(0, 1, 0);
    assertEquals(this.board.getGrid(0, 0).getPlayerID(), 1);
    assertEquals(this.board.getGrid(0, 0).getWorkerID(), 0);
    assertEquals(this.board.getGrid(1, 0).getPlayerID(), 0);
    assertEquals(this.board.getGrid(1, 0).getWorkerID(), 0);
  }

  /*
   * test Apollo can correctly swap other player's workers diagonally
   */
  @Test
  public void testPushDiagonal() {
    assertEquals(this.board.possibleSwaps(0, 0, 0).size(), 2);
    assertEquals(this.board.getGrid(1, 0).getPlayerID(), 1);
    assertEquals(this.board.getGrid(1, 0).getWorkerID(), 0);
    this.game.moveWorker(0, 1, 1);
    assertEquals(this.board.getGrid(0, 0).getPlayerID(), 1);
    assertEquals(this.board.getGrid(0, 0).getWorkerID(), 1);
    assertEquals(this.board.getGrid(1, 1).getPlayerID(), 0);
    assertEquals(this.board.getGrid(1, 1).getWorkerID(), 0);
  }

  /*
   * test Apollo cannot swap a worker of its own
   */
  @Test
  public void testSwapSelfWorker() {
    assertEquals(this.board.possibleCharges(0, 0, 0).size(), 2);
    assertEquals(this.board.getGrid(0, 1).getPlayerID(), 0);
    assertEquals(this.board.getGrid(0, 1).getWorkerID(), 1);
    this.game = this.game.moveWorker(0, 0, 1);
    assertEquals(this.board.getGrid(0, 0).getPlayerID(), 0);
    assertEquals(this.board.getGrid(0, 0).getWorkerID(), 0);
    assertEquals(this.board.getGrid(0, 1).getPlayerID(), 0);
    assertEquals(this.board.getGrid(0, 1).getWorkerID(), 1);
  }
}
