package edu.cmu.cs214.hw3.Player;

import org.junit.Before;
import org.junit.Test;

import edu.cmu.cs214.hw3.Game;
import edu.cmu.cs214.hw3.Board.Board;
import edu.cmu.cs214.hw3.Board.Grid;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

public class PlayerTest {
  Game game;
  Board board;
  List<Player> players = new ArrayList<>();

  @Before
  public void setUp() throws Exception {
    board = new Board();
    Player playerOne = new Mortal(0);
    Player playerTwo = new Mortal(1);
    players.add(playerOne);
    players.add(playerTwo);
    this.game = new Game(board, players);
    this.game = this.game.addWorkerForPlayer(0, 0, 0, 0);
    this.game = this.game.addWorkerForPlayer(1, 0, 1, 1);
  }

  /**
   * test initializeWorker()
   * Using row that is out of range should throw illegal argument exception.
   * 
   * @throws Exception
   */
  @Test
  public void testInitializeWorkerIllegalRow() {
    assertFalse(players.get(0).initializeWorker(board, 0, 5, 0));
  }

  /**
   * test initializeWorker()
   * Using col that is out of range should throw illegal argument exception.
   * 
   * @throws Exception
   */
  @Test
  public void testInitializeWorkerIllegalCol() {
    assertFalse(players.get(0).initializeWorker(board, 0, 0, 5));
  }

  /**
   * test initializeWorker()
   * Occupied place should throw illegal argument exception.
   * 
   * @throws Exception
   */
  @Test
  public void testInitializeWorkerIllegalGrid() {
    assertFalse(players.get(0).initializeWorker(board, 1, 0, 0));
  }

  /**
   * test initializeWorker()
   * Unoccupied place should be occupied after initializing a worker.
   * 
   * @throws Exception
   */
  @Test
  public void testInitializeWorker() {
    assertFalse(board.getGrid(2, 2).getOccupied());
    this.game.addWorkerForPlayer(0, 1, 2, 2);
    assertTrue(board.getGrid(2, 2).getOccupied());
  }

  /**
   * test getWorkerWithID()
   * return worker should have the expected row and col.
   */
  @Test
  public void testGetWorkerWithID() throws Exception {
    Worker worker = this.game.getPlayerWithID(0).getWorkerWithID(0);
    assertEquals(worker.getGrid().getRow(), 0);
    assertEquals(worker.getGrid().getCol(), 0);

    worker = this.game.getPlayerWithID(1).getWorkerWithID(0);
    assertEquals(worker.getGrid().getRow(), 1);
    assertEquals(worker.getGrid().getCol(), 1);
  }

  /**
   * test moveWorker()
   * worker can move to an unoccupied grid.
   */
  @Test
  public void testMoveWorkerSuccess() {
    this.game = this.game.moveWorker(0, 0, 1);
    assertEquals(this.players.get(0).getWorkerWithID(0).getGrid().getRow(), 0);
    assertEquals(this.players.get(0).getWorkerWithID(0).getGrid().getCol(), 1);
  }

  /**
   * test moveWorker()
   * worker cannot move to an occupied grid.
   */
  @Test
  public void testMoveWorkerOccupied() {
    assertTrue(this.game.getBoard().getGrid(0, 0).getOccupied());
    assertTrue(this.game.getBoard().getGrid(1, 1).getOccupied());
    this.game = this.game.moveWorker(0, 1, 1);
    assertEquals(this.players.get(0).getWorkerWithID(0).getGrid().getRow(), 0);
    assertEquals(this.players.get(0).getWorkerWithID(0).getGrid().getCol(), 0);
  }

  /**
   * test moveWorker()
   * worker cannot move to grids that are not adjacent.
   */
  @Test
  public void testMoveWorkerTooFar() {
    this.game = this.game.moveWorker(0, 3, 3);
    assertEquals(this.players.get(0).getWorkerWithID(0).getGrid().getRow(), 0);
    assertEquals(this.players.get(0).getWorkerWithID(0).getGrid().getCol(), 0);
  }

  /**
   * test moveWorker()
   * worker can move one level up.
   */
  @Test
  public void testMoveWorkerUp() {
    Grid grid = board.getGrid(0, 1);
    grid.buildTower();
    this.game = this.game.moveWorker(0, 0, 1);
    assertEquals(this.players.get(0).getWorkerWithID(0).getGrid().getRow(), 0);
    assertEquals(this.players.get(0).getWorkerWithID(0).getGrid().getCol(), 1);
  }

  /**
   * test moveWorker()
   * worker cannot move multiple levels up.
   */
  @Test
  public void testMoveWorkerUpMulti() {
    Grid grid = board.getGrid(0, 1);
    grid.buildTower();
    grid.buildTower();
    this.game = this.game.moveWorker(0, 0, 1);
    assertEquals(this.players.get(0).getWorkerWithID(0).getGrid().getRow(), 0);
    assertEquals(this.players.get(0).getWorkerWithID(0).getGrid().getCol(), 0);
  }

  /**
   * test moveWorker()
   * worker can move one levels down.
   */
  @Test
  public void testMoveWorkerDown() {
    Grid grid = board.getGrid(0, 0);
    grid.buildTower();
    this.game = this.game.moveWorker(0, 0, 1);
    assertEquals(this.players.get(0).getWorkerWithID(0).getGrid().getRow(), 0);
    assertEquals(this.players.get(0).getWorkerWithID(0).getGrid().getCol(), 1);
  }

  /**
   * test moveWorker()
   * worker can move multiple levels down.
   */
  @Test
  public void testMoveWorkerDownMulti() {
    Grid grid = board.getGrid(0, 0);
    grid.buildTower();
    grid.buildTower();
    this.game = this.game.moveWorker(0, 0, 1);
    assertEquals(this.players.get(0).getWorkerWithID(0).getGrid().getRow(), 0);
    assertEquals(this.players.get(0).getWorkerWithID(0).getGrid().getCol(), 1);
  }

  /**
   * test buildItem()
   * worker can build on adjacent grid.
   */
  @Test
  public void testBuildItem() {
    this.game = this.game.buildTower(0, 0, 1);
    assertEquals(this.board.getGrid(0, 1).getLevels().getLevel(), 1);
  }

  /**
   * test buildItem()
   * worker cannot build on occupied grid.
   */
  @Test
  public void testBuildItemOccupied() {
    this.game.buildTower(0, 1, 1);
    assertEquals(this.board.getGrid(1, 1).getLevels().getLevel(), 0);
  }

  /**
   * test buildItem()
   * worker cannot build on grids that is not adjacent.
   */
  @Test
  public void testBuildItemTooFar() {
    this.game.buildTower(0, 3, 3);
    assertEquals(this.board.getGrid(3, 3).getLevels().getLevel(), 0);
  }

  /**
   * test buildItem()
   * A grid can only be built four times (till Dome).
   */
  @Test
  public void testBuildItemDome() {
    this.game.buildTower(0, 0, 1);
    this.game.buildTower(0, 0, 1);
    this.game.buildTower(0, 0, 1);
    this.game.buildTower(0, 0, 1);
    this.game.buildTower(0, 0, 1);
    assertEquals(this.board.getGrid(0, 1).getLevels().getLevel(), 4);
  }
}
