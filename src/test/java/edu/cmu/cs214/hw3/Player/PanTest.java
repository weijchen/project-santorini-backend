package edu.cmu.cs214.hw3.Player;

import org.junit.Before;
import org.junit.Test;

import edu.cmu.cs214.hw3.Game;
import edu.cmu.cs214.hw3.Board.Board;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

public class PanTest {
  Game game;
  Board board;
  List<Player> players = new ArrayList<>();

  @Before
  public void setUp() throws Exception {
    board = new Board();
    Player playerOne = new Pan(0);
    Player playerTwo = new Mortal(1);
    players.add(playerOne);
    players.add(playerTwo);
    this.game = new Game(board, players);
    this.game = this.game.addWorkerForPlayer(0, 0, 0, 0);
    this.game = this.game.addWorkerForPlayer(1, 0, 1, 1);
  }

  @Test
  public void testSpecialWinCondition() {
    this.game = this.game.moveWorker(0, 0, 1);
    this.game = this.game.buildTower(0, 0, 0);
    this.game = this.game.moveWorker(0, 0, 0);

    this.game = this.game.buildTower(0, 0, 1);
    this.game = this.game.buildTower(0, 0, 1);
    this.game = this.game.moveWorker(0, 0, 1);

    assertEquals(board.getGrid(0, 1).getLevels().getLevel(), 2);
    assertEquals(board.getGrid(0, 2).getLevels().getLevel(), 0);

    assertFalse(this.game.hasWinner(false));
    this.game = this.game.moveWorker(0, 0, 2);
    assertTrue(this.game.hasWinner(false));
    assertEquals(this.game.getWinnerID(), 0);
  }
}
