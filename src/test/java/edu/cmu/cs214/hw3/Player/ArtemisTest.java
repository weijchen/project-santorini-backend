package edu.cmu.cs214.hw3.Player;

import org.junit.Before;
import org.junit.Test;

import edu.cmu.cs214.hw3.Game;
import edu.cmu.cs214.hw3.Board.Board;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

public class ArtemisTest {
  Game game;
  Board board;
  List<Player> players = new ArrayList<>();

  @Before
  public void setUp() throws Exception {
    board = new Board();
    Player playerOne = new Artemis(0);
    Player playerTwo = new Mortal(1);
    players.add(playerOne);
    players.add(playerTwo);
    this.game = new Game(board, players);
    this.game = this.game.addWorkerForPlayer(0, 0, 0, 0);
    this.game = this.game.addWorkerForPlayer(1, 0, 1, 1);
  }

  @Test
  public void testArtemisMoveDifferent() {
    this.game = this.game.moveWorker(0, 0, 1);
    assertEquals(this.game.getCurPlayer().getWorkerWithID(0).getGrid().getRow(), 0);
    assertEquals(this.game.getCurPlayer().getWorkerWithID(0).getGrid().getCol(), 1);
    this.game = this.game.moveWorker(0, 0, 2);
    assertEquals(this.game.getCurPlayer().getWorkerWithID(0).getGrid().getRow(), 0);
    assertEquals(this.game.getCurPlayer().getWorkerWithID(0).getGrid().getCol(), 2);
  }

  @Test
  public void testArtemisMoveBack() {
    this.game = this.game.moveWorker(0, 0, 1);
    assertEquals(this.game.getCurPlayer().getWorkerWithID(0).getGrid().getRow(), 0);
    assertEquals(this.game.getCurPlayer().getWorkerWithID(0).getGrid().getCol(), 1);
    this.game = this.game.moveWorker(0, 0, 0);
    assertEquals(this.game.getCurPlayer().getWorkerWithID(0).getGrid().getRow(), 0);
    assertEquals(this.game.getCurPlayer().getWorkerWithID(0).getGrid().getCol(), 1);
  }
}
