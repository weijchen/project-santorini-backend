package edu.cmu.cs214.hw3.Player;

import org.junit.Before;
import org.junit.Test;

import edu.cmu.cs214.hw3.Game;
import edu.cmu.cs214.hw3.Board.Board;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

public class DemeterTest {
  Game game;
  Board board;
  List<Player> players = new ArrayList<>();

  @Before
  public void setUp() throws Exception {
    board = new Board();
    Player playerOne = new Demeter(0);
    Player playerTwo = new Mortal(1);
    players.add(playerOne);
    players.add(playerTwo);
    this.game = new Game(board, players);
    this.game = this.game.addWorkerForPlayer(0, 0, 0, 0);
    this.game = this.game.addWorkerForPlayer(1, 0, 1, 1);
  }

  @Test
  public void testDemeterBuildDifferent() {
    this.game = this.game.moveWorker(0, 0, 1);
    this.game = this.game.buildTower(0, 0, 0);
    assertEquals(this.game.getBoard().getGrid(0, 0).getLevels().getLevel(), 1);
    this.game = this.game.buildTower(0, 0, 2);
    assertEquals(this.game.getBoard().getGrid(0, 2).getLevels().getLevel(), 1);
  }

  @Test
  public void testDemeterBuildSame() {
    this.game = this.game.moveWorker(0, 0, 1);
    this.game = this.game.buildTower(0, 0, 0);
    assertEquals(this.game.getBoard().getGrid(0, 0).getLevels().getLevel(), 1);
    this.game = this.game.buildTower(0, 0, 0);
    assertEquals(this.game.getBoard().getGrid(0, 0).getLevels().getLevel(), 1);
  }

  @Test
  public void testDemeterPass() {
    this.game.moveWorker(0, 0, 1);
    this.game.buildTower(0, 0, 0);
    assertEquals(this.game.getBoard().getGrid(0, 0).getLevels().getLevel(), 1);
    this.game.buildTower(0, 0, 1);
    assertEquals(this.game.getBoard().getGrid(0, 0).getLevels().getLevel(), 1);
  }

  @Test
  public void testDemeterBuildDome() {
    this.game.moveWorker(0, 0, 1);
    // level one
    this.game.buildTower(0, 0, 0);
    assertEquals(this.game.getBoard().getGrid(0, 0).getLevels().getLevel(), 1);
    this.game.buildTower(0, 0, 0);
    assertEquals(this.game.getBoard().getGrid(0, 0).getLevels().getLevel(), 1);

    // level two
    this.game.buildTower(0, 0, 0);
    assertEquals(this.game.getBoard().getGrid(0, 0).getLevels().getLevel(), 2);
    this.game.buildTower(0, 0, 0);
    assertEquals(this.game.getBoard().getGrid(0, 0).getLevels().getLevel(), 2);

    // level three
    this.game.buildTower(0, 0, 0);
    assertEquals(this.game.getBoard().getGrid(0, 0).getLevels().getLevel(), 3);
    this.game.buildTower(0, 0, 0);
    assertEquals(this.game.getBoard().getGrid(0, 0).getLevels().getLevel(), 3);

    // dome
    this.game.buildTower(0, 0, 0);
    assertEquals(this.game.getBoard().getGrid(0, 0).getLevels().getLevel(), 4);
    this.game.buildTower(0, 0, 0);
    assertEquals(this.game.getBoard().getGrid(0, 0).getLevels().getLevel(), 4);
  }
}
