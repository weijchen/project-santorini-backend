package edu.cmu.cs214.hw3.Player;

import org.junit.Before;
import org.junit.Test;

import edu.cmu.cs214.hw3.Board.Grid;
import edu.cmu.cs214.hw3.Board.Tower;

import static org.junit.Assert.assertEquals;

public class WorkerTest {
  Worker worker;

  @Before
  public void setUp() throws Exception {
    Grid grid = new Grid(0, 0, false, Tower.None);
    worker = new Worker(0, 0, grid);
  }

  @Test
  public void testUpdateCoordinate() {
    Grid newGrid = new Grid(1, 1, false, Tower.None);
    worker.setGrid(newGrid);
    assertEquals(worker.getGrid().getRow(), 1);
    assertEquals(worker.getGrid().getCol(), 1);
  }
}
