package edu.cmu.cs214.hw3.Board;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

public class BoardTest {
  private Board board;

  @Before
  public void setUp() {
    board = new Board();
  }

  /**
   * test checkGridIsValid()
   * check whether grid is inside the board
   */
  @Test
  public void testCheckGridIsValid() {
    assertTrue(board.checkGridIsValid(0, 0));
    assertFalse(board.checkGridIsValid(6, 6));
  }

  /**
   * test checkGridIsValid()
   * check whether grid is valid based on its occupied status
   */
  @Test
  public void testCheckGridIsValidOccupied() {
    board.getGrid(0, 0).setOccupied(true);
    assertFalse(board.checkGridIsValid(0, 0));
    board.getGrid(0, 0).setOccupied(false);
    assertTrue(board.checkGridIsValid(0, 0));
  }

  /**
   * test possibleMoves()
   * check the size of possible move points is as expected
   */
  @Test
  public void testPossibleMoves() {
    List<Grid> possibleMoves = board.possibleMoves(1, 1);
    assertEquals(possibleMoves.size(), 8);
    board.getGrid(0, 0).setOccupied(true);
    possibleMoves = board.possibleMoves(1, 1);
    assertEquals(possibleMoves.size(), 7);
  }

  /**
   * test possibleMoves()
   * check the size of possible move points from corner is as expected
   */
  @Test
  public void testPossibleMovesCorner() {
    List<Grid> possibleMoves = board.possibleMoves(0, 0);
    assertEquals(possibleMoves.size(), 3);
  }

  /**
   * test possibleMoves()
   * check the rule of moving up only one level
   */
  @Test
  public void testPossibleMovesUp() {
    Grid grid = board.getGrid(0, 0);
    grid.buildTower();
    List<Grid> possibleMoves = board.possibleMoves(1, 1);
    assertEquals(possibleMoves.size(), 8);
    grid.buildTower();
    possibleMoves = board.possibleMoves(1, 1);
    assertEquals(possibleMoves.size(), 7);
  }

  /**
   * test possibleMoves()
   * check the rule of moving down any levels
   */
  @Test
  public void testPossibleMovesDown() {
    Grid grid = board.getGrid(1, 1);
    grid.buildTower();
    List<Grid> possibleMoves = board.possibleMoves(1, 1);
    assertEquals(possibleMoves.size(), 8);
    grid.buildTower();
    possibleMoves = board.possibleMoves(1, 1);
    assertEquals(possibleMoves.size(), 8);
    grid.buildTower();
    possibleMoves = board.possibleMoves(1, 1);
    assertEquals(possibleMoves.size(), 8);
  }

  /**
   * test possibleBuilds()
   * check the size of possible build points is as expected
   */
  @Test
  public void testPossibleBuilds() {
    List<Grid> possibleBuilds = board.possibleBuilds(1, 1);
    assertEquals(possibleBuilds.size(), 8);
    board.getGrid(0, 0).setOccupied(true);
    possibleBuilds = board.possibleBuilds(1, 1);
    assertEquals(possibleBuilds.size(), 7);
  }

  /**
   * test possibleBuilds()
   * check the size of possible build points from corner is as expected
   */
  @Test
  public void testPossibleBuildsCorner() {
    List<Grid> possibleBuilds = board.possibleBuilds(0, 0);
    assertEquals(possibleBuilds.size(), 3);
  }

  /**
   * test possibleBuilds()
   * a grid with dome built cannot be built again
   */
  @Test
  public void testPossibleBuildsDome() {
    Grid grid = board.getGrid(1, 1);
    grid.buildTower();
    List<Grid> possibleBuilds = board.possibleBuilds(0, 0);
    assertEquals(possibleBuilds.size(), 3);
    grid.buildTower();
    grid.buildTower();
    grid.buildTower();
    possibleBuilds = board.possibleBuilds(0, 0);
    assertEquals(possibleBuilds.size(), 2);
  }
}
