package edu.cmu.cs214.hw3.Board;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GridTest {
  Board board;

  @Before
  public void setUp() {
    board = new Board();
  }

  /**
   * test buildTower()
   * Check level of the grid is advancing. The grid should be occupied once a dome
   * is built.
   */
  @Test
  public void testBuildTower() {
    Grid grid = board.getGrid(0, 0);
    assertEquals(grid.getLevels(), Tower.None);
    assertFalse(grid.getOccupied());
    grid.buildTower();
    assertEquals(grid.getLevels(), Tower.First);
    assertFalse(grid.getOccupied());
    grid.buildTower();
    assertEquals(grid.getLevels(), Tower.Second);
    assertFalse(grid.getOccupied());
    grid.buildTower();
    assertEquals(grid.getLevels(), Tower.Third);
    assertFalse(grid.getOccupied());
    grid.buildTower();
    assertEquals(grid.getLevels(), Tower.Dome);
    assertTrue(grid.getOccupied());
  }
}
