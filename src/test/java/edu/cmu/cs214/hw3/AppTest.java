package edu.cmu.cs214.hw3;

import org.junit.Before;
import org.junit.Test;

import edu.cmu.cs214.hw3.Board.Board;
import edu.cmu.cs214.hw3.Player.Mortal;
import edu.cmu.cs214.hw3.Player.Player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest {
    Game game;
    Board board;
    List<Player> players = new ArrayList<>();
    public static final int WORKER_ONE_PLAYER_ONE_X = 0;
    public static final int WORKER_ONE_PLAYER_ONE_Y = 0;
    public static final int WORKER_TWO_PLAYER_ONE_X = 0;
    public static final int WORKER_TWO_PLAYER_ONE_Y = 4;

    public static final int WORKER_ONE_PLAYER_TWO_X = 4;
    public static final int WORKER_ONE_PLAYER_TWO_Y = 0;
    public static final int WORKER_TWO_PLAYER_TWO_X = 4;
    public static final int WORKER_TWO_PLAYER_TWO_Y = 4;

    @Before
    public void setUp() throws Exception {
        board = new Board();
        Player playerOne = new Mortal(0);
        Player playerTwo = new Mortal(1);
        players.add(playerOne);
        players.add(playerTwo);

        this.game = new Game(board, players);
        this.game.addWorkerForPlayer(0, 0, WORKER_ONE_PLAYER_ONE_X, WORKER_ONE_PLAYER_ONE_Y);
        this.game.addWorkerForPlayer(0, 1, WORKER_TWO_PLAYER_ONE_X, WORKER_TWO_PLAYER_ONE_Y);
        this.game.addWorkerForPlayer(1, 0, WORKER_ONE_PLAYER_TWO_X, WORKER_ONE_PLAYER_TWO_Y);
        this.game.addWorkerForPlayer(1, 1, WORKER_TWO_PLAYER_TWO_X, WORKER_TWO_PLAYER_TWO_Y);
    }

    /**
     * integration test
     * this test should indicate that player one is the winner and the game is over
     */
    @Test
    public void testHasWinner() {

        this.game = this.game.moveWorker(0, 0, 1);
        this.game = this.game.buildTower(0, 0, 0);
        this.game = this.game.takeTurn();
        this.game = this.game.moveWorker(1, 4, 3);
        this.game = this.game.buildTower(1, 4, 4);
        this.game = this.game.takeTurn();

        this.game = this.game.moveWorker(0, 1, 1);
        this.game = this.game.buildTower(0, 0, 0);
        this.game = this.game.takeTurn();
        this.game = this.game.moveWorker(1, 3, 3);
        this.game = this.game.buildTower(1, 4, 4);
        this.game = this.game.takeTurn();

        this.game = this.game.moveWorker(0, 1, 0);
        this.game = this.game.buildTower(0, 0, 0);
        this.game = this.game.takeTurn();
        this.game = this.game.moveWorker(1, 3, 4);
        this.game = this.game.buildTower(1, 4, 4);
        this.game = this.game.takeTurn();

        this.game = this.game.moveWorker(0, 0, 1);
        this.game = this.game.buildTower(0, 1, 1);
        this.game = this.game.takeTurn();
        this.game = this.game.moveWorker(1, 4, 3);
        this.game = this.game.buildTower(1, 3, 3);
        this.game = this.game.takeTurn();

        this.game = this.game.moveWorker(0, 1, 1);
        this.game = this.game.buildTower(0, 0, 1);
        this.game = this.game.takeTurn();
        this.game = this.game.moveWorker(1, 3, 3);
        this.game = this.game.buildTower(1, 3, 4);
        this.game = this.game.takeTurn();

        this.game = this.game.moveWorker(0, 0, 1);
        this.game = this.game.buildTower(0, 1, 1);
        this.game = this.game.takeTurn();
        this.game = this.game.moveWorker(1, 3, 4);
        this.game = this.game.buildTower(1, 3, 3);
        this.game = this.game.takeTurn();

        this.game = this.game.moveWorker(0, 1, 1);
        this.game = this.game.buildTower(0, 0, 1);
        this.game = this.game.takeTurn();
        this.game = this.game.moveWorker(1, 3, 3);
        this.game = this.game.buildTower(1, 3, 4);
        this.game = this.game.takeTurn();

        this.game = this.game.moveWorker(0, 0, 0);

        assertTrue(this.game.hasWinner(false));
        assertEquals(this.game.getWinnerID(), 0);
        assertTrue(this.game.getPlayerWithID(0).getWin());
        assertFalse(this.game.getPlayerWithID(1).getWin());
    }

    /**
     * integration test
     * this test should indicate that there is no winner, the current winner is
     * 0 (no winner), and the game is not over yet
     */
    @Test
    public void testHasNoWinner() {

        this.game = this.game.moveWorker(0, 0, 1);
        this.game = this.game.buildTower(0, 0, 0);
        this.game = this.game.takeTurn();
        this.game = this.game.moveWorker(1, 4, 3);
        this.game = this.game.buildTower(1, 4, 4);
        this.game = this.game.takeTurn();

        this.game = this.game.moveWorker(0, 1, 1);
        this.game = this.game.buildTower(0, 0, 0);
        this.game = this.game.takeTurn();
        this.game = this.game.moveWorker(1, 3, 3);
        this.game = this.game.buildTower(1, 4, 4);
        this.game = this.game.takeTurn();

        this.game = this.game.moveWorker(0, 1, 0);
        this.game = this.game.buildTower(0, 0, 0);
        this.game = this.game.takeTurn();
        this.game = this.game.moveWorker(1, 3, 4);
        this.game = this.game.buildTower(1, 4, 4);
        this.game = this.game.takeTurn();

        this.game = this.game.moveWorker(0, 0, 1);
        this.game = this.game.buildTower(0, 1, 1);
        this.game = this.game.takeTurn();
        this.game = this.game.moveWorker(1, 4, 3);
        this.game = this.game.buildTower(1, 3, 3);
        this.game = this.game.takeTurn();

        this.game = this.game.moveWorker(0, 1, 1);
        this.game = this.game.buildTower(0, 0, 1);
        this.game = this.game.takeTurn();
        this.game = this.game.moveWorker(1, 3, 3);
        this.game = this.game.buildTower(1, 3, 4);
        this.game = this.game.takeTurn();

        this.game = this.game.moveWorker(0, 0, 1);
        this.game = this.game.buildTower(0, 1, 1);
        this.game = this.game.takeTurn();
        this.game = this.game.moveWorker(1, 3, 4);
        this.game = this.game.buildTower(1, 3, 3);
        this.game = this.game.takeTurn();

        this.game = this.game.moveWorker(0, 1, 1);
        this.game = this.game.buildTower(0, 0, 1);
        this.game = this.game.takeTurn();
        this.game = this.game.moveWorker(1, 3, 3);
        this.game = this.game.buildTower(1, 3, 4);
        this.game = this.game.takeTurn();

        assertFalse(this.game.hasWinner(false));
        assertEquals(this.game.getWinnerID(), -1);
    }

    /**
     * test takeTurn()
     * when calling takeTurn(), Game should update the state of current player
     */
    @Test
    public void testTakeTurn() {
        assertEquals(game.getCurPlayerID(), 0);
        this.game = this.game.takeTurn();
        assertEquals(game.getCurPlayerID(), 1);
        this.game = this.game.takeTurn();
        assertEquals(game.getCurPlayerID(), 0);
    }
}
